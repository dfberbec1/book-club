package com.oocode;

public class BookClassic extends Book {

    public BookClassic(String bookTitle) {
        super(bookTitle);
    }

    public BookClassic(String bookTitle, String review) {
        super(bookTitle);
    }
}
