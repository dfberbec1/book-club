package com.oocode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Book {

    String bookTitle;
    List reviews = new LinkedList();

    public Book(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public void addReview(String review) {
        reviews.add(review);
    }

    public boolean isPartOfSearch(String bookQuery) {
        boolean isUppercase = bookQuery.toUpperCase().equals(bookQuery);
        return (isUppercase? transformToInitials(): bookTitle).toLowerCase().startsWith(bookQuery.toLowerCase());
    }

    public String transformToInitials() {
        return Arrays.stream(bookTitle.split(" "))
                .map(word -> String.valueOf(word.charAt(0)))
                .collect(Collectors.joining());
    }

    public List getReviews() {
        return reviews;
    }
}
