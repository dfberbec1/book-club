package com.oocode;

import java.util.Date;

public class BookRegular extends Book{

    Long lastTimestamp = Long.MIN_VALUE;
    private static final int MILLIS_IN_SECOND = 1000;
    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MINUTES_IN_HOUR = 60;
    private static final int HOURS_IN_DAY = 24;
    private static final int DAYS_IN_YEAR = 365;
     static final long MILLISECONDS_IN_YEAR = (long) MILLIS_IN_SECOND * SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_YEAR;

    public BookRegular(String bookTitle) {
        super(bookTitle);
    }

    public BookRegular(String bookTitle, String review) {
        super(bookTitle);
        addReview(review);
    }

    @Override
    public void addReview(String review) {
        super.addReview(review);
        lastTimestamp = new Date().getTime();
    }

    @Override
    public boolean isPartOfSearch(String bookQuery) {
        return super.isPartOfSearch(bookQuery) && isRecentlyReviewed();
    }

    private boolean isRecentlyReviewed() {
        return new Date().getTime() - lastTimestamp < MILLISECONDS_IN_YEAR;
    }
}
