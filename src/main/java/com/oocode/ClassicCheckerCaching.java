package com.oocode;

import java.util.LinkedHashMap;
import java.util.Map;

public class ClassicCheckerCaching implements ClassicChecker{
    ClassicChecker classicChecker;
    LinkedHashMap<String, Boolean> classicCacheMap = new LinkedHashMap<String, Boolean>(10, (float) 0.75, true) {
        private static final int MAX_ENTRIES = 1000;

        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > MAX_ENTRIES;
        }
    };

    public ClassicCheckerCaching(ClassicChecker classicChecker) {
        this.classicChecker = classicChecker;
    }


    @Override
    public boolean isClassic(String bookTitle) {
        classicCacheMap.computeIfAbsent(bookTitle, k -> classicChecker.isClassic(k));
        return classicCacheMap.get(bookTitle);
    }
}
