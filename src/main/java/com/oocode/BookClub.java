package com.oocode;

import java.util.*;
import java.util.stream.Collectors;

public class BookClub {
    private final Map<String, Book> booksMap = new HashMap<>();
    private final ClassicChecker classicChecker;

    public BookClub(ClassicChecker classicChecker) {
        this.classicChecker = classicChecker;
    }

    public void addReview(String bookTitle, String review) {
        if (!booksMap.containsKey(bookTitle)) {
            if (classicChecker.isClassic(bookTitle)) {
                booksMap.put(bookTitle, new BookClassic(bookTitle, review));
            } else {
                booksMap.put(bookTitle, new BookRegular(bookTitle, review));
            }
        } else {
            booksMap.get(bookTitle).addReview(review);
        }
    }

    public List<String> reviewsFor(String bookTitle) {
        if (booksMap.containsKey(bookTitle)) {
            return booksMap.get(bookTitle).getReviews();
        }
        return Collections.emptyList();
    }

    public List<String> classics(String n) {
        return search(n).stream().filter(key -> booksMap.get(key) instanceof BookClassic).sorted()
                .collect(Collectors.toList());
    }

    public List<String> search(String bookTitle) {
        if (bookTitle== null || bookTitle.isEmpty()) {
            throw new IllegalArgumentException();
        }

        return booksMap.values().stream()
                .filter(book -> book.isPartOfSearch(bookTitle))
                .map(book -> book.bookTitle)
                .sorted()
                .collect(Collectors.toList());
    }
}
