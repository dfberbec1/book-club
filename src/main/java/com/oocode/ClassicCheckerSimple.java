package com.oocode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class ClassicCheckerSimple implements ClassicChecker {
    private static final String defaultUrl = "https://pure-coast-78546.herokuapp.com/isClassic/";

    public URL createURL(String bookTitle) throws MalformedURLException {
        try {
            return new URL(defaultUrl + URLEncoder.encode(bookTitle, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isClassic(String bookTitle){
        URLConnection conn = null;
        try {
            conn = createURL(bookTitle).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                conn.getInputStream(), StandardCharsets.UTF_8))) {
            return reader.lines().collect(Collectors.joining("\n"))
                    .contains("true");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
