package com.oocode;

import java.net.MalformedURLException;

public interface ClassicChecker {

//    public String createURL(String bookTitle) throws MalformedURLException;
    public boolean isClassic(String bookTitle);
}
