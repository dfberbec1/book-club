package com.oocode;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookClubTest {
    BookClub bookClub;
    ClassicChecker classicChecker;

    @Before
    public void setUp() {
         classicChecker = mock(ClassicChecker.class);
        bookClub = new BookClub(classicChecker);
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchInputShouldNotBeNull() {
        bookClub.search(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchInputShouldNotBeEmpty() {
        bookClub.search("");
    }

    @Test
    public void searchStartOfTitle_notAllUpperCase() {
        bookClub.addReview("Hello World", "great");
        bookClub.addReview("Hello Wally", "boring");

        String errorMessage = "The book Hello Wally is alphabetically before Hello World";
        List<String> expectedList = asList("Hello Wally", "Hello World");

        assertEquals(errorMessage, expectedList, bookClub.search("Hel"));
        assertEquals(expectedList, bookClub.search("hel"));
    }

    @Test
    public void canSearchByInitials_allUpperCase() {
        bookClub.addReview("Hallo World", "great");
        bookClub.addReview("Hello World", "great");
        bookClub.addReview("Hello Wally", "boring");
        bookClub.addReview("Hallo Wally", "boring");

        List<String> expectedList = asList("Hallo Wally", "Hallo World", "Hello Wally", "Hello World");
        assertEquals("Results when searching by initials should be alphabetically ordered.",
                expectedList, bookClub.search("HW"));
    }

    @Test
    public void searchStartOfTitelandInitials() {
        bookClub.addReview("hello world", "great");
        bookClub.addReview("hello wally", "boring");

        List<String> expectedList = asList("hello wally", "hello world");
        assertEquals(expectedList, bookClub.search("Hel"));
        assertEquals(expectedList, bookClub.search("hel"));
        assertEquals(expectedList, bookClub.search("HW"));
    }

    @Test
    public void reviewsFor() {
        bookClub.addReview("Hello World", "great");
        bookClub.addReview("Hello World", "the best book ever");

        assertEquals(asList("great", "the best book ever"),
                bookClub.reviewsFor("Hello World"));
    }

    @Test
    public void classics() {
        when(classicChecker.isClassic("Hello World")).thenReturn(true);
        when(classicChecker.isClassic("Hello Wally")).thenReturn(false);

        bookClub.addReview("Hello World", "great");
        bookClub.addReview("Hello Wally", "boring");


        assertEquals(asList("Hello World"), bookClub.classics("HW"));
        assertEquals(asList("Hello World"), bookClub.classics("He"));
    }
}