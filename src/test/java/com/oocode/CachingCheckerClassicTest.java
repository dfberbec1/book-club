package com.oocode;

import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CachingCheckerClassicTest {
    ClassicChecker classicChecker = mock(ClassicChecker.class);

    @Test
    public void isClassicSameCallHitsCache(){
        ClassicCheckerCaching classicCheckerCaching = new ClassicCheckerCaching(classicChecker);

        classicCheckerCaching.isClassic("some book title");
        classicCheckerCaching.isClassic("some book title");

        verify(classicChecker, times(1)).isClassic(any());

    }
}
