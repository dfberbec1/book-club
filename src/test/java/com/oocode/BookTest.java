package com.oocode;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTest {

    @Test
    public void isPartOfSearchClassicBook(){
        Book bookClassic = new BookClassic("some classic book");
        String searchInput = "some";
        assertTrue(bookClassic.isPartOfSearch(searchInput));
        searchInput = "son";
        assertFalse(bookClassic.isPartOfSearch(searchInput));
        searchInput = "SC";
        assertTrue(bookClassic.isPartOfSearch(searchInput));
    }

    @Test
    public void isPartOfSearchRegularBookWithRecentReview(){
        Book bookRegular = new BookRegular("some regular book", "good");
        String searchInput = "some";
        assertTrue(bookRegular.isPartOfSearch(searchInput));
        searchInput = "son";
        assertFalse(bookRegular.isPartOfSearch(searchInput));
        searchInput = "SR";
        assertTrue(bookRegular.isPartOfSearch(searchInput));
    }

    @Test
    public void isPartOfSearchRegularBookWithNoRecentReview(){
        BookRegular bookRegular = new BookRegular("some regular book", "good") {
            @Override
            public void addReview(String review) {
                reviews.add(review);
                lastTimestamp = new Date().getTime() - (1 * MILLISECONDS_IN_YEAR);
            }
        };
        String searchInput = "some";
        assertFalse(bookRegular.isPartOfSearch(searchInput));
        searchInput = "son";
        assertFalse(bookRegular.isPartOfSearch(searchInput));
        searchInput = "SR";
        assertFalse(bookRegular.isPartOfSearch(searchInput));
    }
}
