package com.oocode;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class ClassicCheckerTest {
    URL url = mock(URL.class);
    URLConnection conn = mock(URLConnection.class);

    @Test
    public void isClassic() throws IOException {
        ClassicCheckerSimple classicChecker = new ClassicCheckerSimple(){

            @Override
            public URL createURL(String bookTitle) throws MalformedURLException {
                return url;
            }
        };

        when(url.openConnection()).thenReturn(conn);
        InputStream inputStreamTrue = new ByteArrayInputStream("true".getBytes());
        InputStream inputStreamFalse = new ByteArrayInputStream("false".getBytes());
        when(conn.getInputStream()).thenReturn(inputStreamTrue).thenReturn(inputStreamFalse);

        assertTrue(classicChecker.isClassic("some title"));
        assertFalse(classicChecker.isClassic("some title"));

        verify(conn, times(2)).getInputStream();
    }
}
